﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson1_task1_1
{
    class Address
    {
        private string index, country, city, street, house, apartment;
        public string Index
        {
            set
            {
                index = value;
            }
            get
            {
                return index;
            }
        }
        public string Country
        {
            set
            {
                country = value;
            }
            get
            {
                return country;
            }
        }
        public string City
        {
            set
            {
                city = value;
            }
            get
            {
                return city;
            }
        }
        public string Street
        {
            set
            {
                street = value;
            }
            get
            {
                return street;
            }
        }
        public string House
        {
            set
            {
                house = value;
            }
            get
            {
                return house;
            }
        }
        public string Apartment
        {
            set
            {
                apartment = value;
            }
            get
            {
                return apartment;
            }
        }

        public void ShowAddress()
        {
            Console.WriteLine($"Индекс: {index} Страна: {country} Город: {city} Улица: {city} Дом: {house} Квартира: {apartment}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Address address = new Address();

            Console.WriteLine("Здравствуйте, введите адрес!");

            Console.Write("Индекс: \t");
            address.Index = Console.ReadLine();

            Console.Write("Страна: \t");
            address.Country = Console.ReadLine();

            Console.Write("Город: \t");
            address.City = Console.ReadLine();

            Console.Write("Улица: \t");
            address.Street = Console.ReadLine();

            Console.Write("Дом: \t");
            address.House = Console.ReadLine();

            Console.Write("Квартира: \t");
            address.Apartment = Console.ReadLine();

            address.ShowAddress();
            Console.ReadKey();

        }
    }
}
